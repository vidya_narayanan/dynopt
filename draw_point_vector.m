function [ ] = draw_point_vector( p, v, color )
a = p;
b = p + v;
C = [a, b];
plot3(C(1,:), C(2,:), C(3,:), 'color', color);
end

