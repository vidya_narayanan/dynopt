function [r, p, y] = part1( target, link_length, min_roll, max_roll, min_pitch, max_pitch, min_yaw, max_yaw, obstacles_ )
%% Function that uses optimization to do inverse kinematics for a snake robot

%%Outputs 
  % [r, p, y] = roll, pitch, yaw vectors of the N joint angles
  %            (N link coordinate frames)
%%Inputs:
    % target: [x, y, z, q0, q1, q2, q3]' position and orientation of the end
    %    effector
    % link_length : Nx1 vectors of the lengths of the links
    % min_xxx, max_xxx are the vectors of the 
    %    limits on the roll, pitch, yaw of each link.
    % limits for a joint could be something like [-pi, pi]
    % obstacles: A Mx4 matrix where each row is [ x y z radius ] of a sphere
    %    obstacle. M obstacles.

% Your code goes here.
global roll_min roll_max pitch_min pitch_max yaw_min yaw_max
global link_lengths target_pt
global obstacles 
global soft_penalty
link_lengths = link_length;
target_pt = target;
roll_min = min_roll;
roll_max = max_roll;
pitch_min = min_pitch;
pitch_max = max_pitch;
yaw_min = min_yaw;
yaw_max = max_yaw;
obstacles = obstacles_;
soft_penalty = false;
N  = length(link_lengths);

initdraw();
%start == joint angles/quaternion
%start with some joint angles for each link

start = zeros(3*N,1); %start with 0 angles

%hard bounds constraint
if soft_penalty
    lb = -inf*ones(size(start));
    ub = inf*ones(size(start));
else
    lb = [yaw_min pitch_min roll_min]';
    ub = [yaw_max pitch_max roll_max]';
end
%if using soft constraints:
global algorithm
global alg_time
global alg_fval
global alg_answer

algorithm='interior-point'

if strcmp(algorithm, 'cma-es')
    opts.LBounds = lb; opts.UBounds = ub;
    opts.StopFitness = 1e-3;
    opts.MaxIter = 1e+3;
    [answer, fval] = cmaes('criterion',start,.5,opts);
    alg_time = toc;
    alg_fval = fval;
    alg_answer = answer;  
else
    options = optimoptions(@fmincon,'Algorithm',algorithm);
    tic;
    [answer,fval] = fmincon(@criterion, start,[],[],[],[],lb,ub,[],options);
    alg_time = toc;
    alg_fval = fval;
    alg_answer = answer;
end
%return joint angles
r = answer(2*N+1:3*N)';
p = answer(N+1:2*N)';
y = answer(1:N)';
if any(r < roll_min) || any(r > roll_max)
    fprintf('roll bounds not satisfied \n');
end
if any(p < pitch_min) || any(p > pitch_max)
    fprintf('pitch bounds not sastisfied\n');
end
if any(y < yaw_min) || any(y > yaw_max)
    fprintf('yaw bounds not satisfied\n');
end

global target_sphere h_axes
if fval < 0.01 %actually just check target pos
    set(target_sphere,'Parent',h_axes,'FaceColor','green','visible','on');
else
    set(target_sphere,'Parent',h_axes,'FaceColor','red','visible','on');

end


end
