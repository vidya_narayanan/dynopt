function [ result ] = dtds_computer(ii, jj, angles)
% ii \in 1...,N (joints)
% jj \in 1, 2, 3 -> alpha, beta, gamma
global link_lengths
N = length(link_lengths);

roll = angles(2*N+1:3*N);
pitch = angles(N+1:2*N);
yaw = angles(1:N);

Rx = @(r) [1 0 0 ; 0 cos(r) -sin(r) ; 0 sin(r) cos(r)];
Ry = @(p) [cos(p) 0 sin(p) ; 0 1 0 ; -sin(p) 0 cos(p)];
Rz = @(y) [cos(y) -sin(y) 0 ; sin(y) cos(y) 0 ; 0 0 1];

Rxp = @(r) [0 0 0 ; 0 -sin(r) -cos(r) ; 0 cos(r) -sin(r)];
Ryp = @(p) [-sin(p) 0 cos(p) ; 0 0 0 ; -cos(p) 0 -sin(p)];
Rzp = @(y) [-sin(y) -cos(y) 0 ; cos(y) -sin(y) 0 ; 0 0 0];

cummR = zeros(3,3,N);
for i = 1:N
    r = roll(i);
    p = pitch(i);
    y = yaw(i);
    
    RRx = Rx(r);
    RRy = Ry(p);
    RRz = Rz(y);
   
    % Derivative cases.
    if (i == ii)
       if (jj == 1)
           RRx = Rxp(r);
       elseif (jj == 2)
           RRy = Ryp(p);
       elseif jj == 3
           RRz = Rzp(y);
       end
    end
       
    R = RRx*RRy*RRz;
    if i == 1
        cummR(:,:,i) = R;
    else
        cummR(:,:,i) = R*cummR(:,:,i-1);
    end
end

result = zeros(3, 1);
for i = ii:N 
    result = result + cummR(:,:, i)*[link_lengths(i) 0 0]';
end

end

