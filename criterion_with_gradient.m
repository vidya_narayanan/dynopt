function [score, gradient] = criterion_with_gradient( angles )
global link_lengths target_pt
global obstacles
global roll_min roll_max pitch_min pitch_max yaw_min yaw_max

%CRITERION Summary of this function goes here
%   Detailed explanation goes here

N = length(link_lengths);
joint_angles_roll = angles(2*N+1:3*N);
joint_angles_pitch = angles(N+1:2*N);
joint_angles_yaw = angles(1:N);
fk_position = fk(joint_angles_yaw, joint_angles_pitch, joint_angles_roll);

%% Compute score.
target_tip_pos = target_pt(1:3);
actual_tip_pos  = fk_position(N,1:3)';
D_position = actual_tip_pos - target_tip_pos;

actual_tip_vec = actual_tip_pos - fk_position(N-1,1:3)';
actual_tip_vec = actual_tip_vec / link_lengths(N);
target_tip_vec = quatrotate(target_pt(4:7)', [1, 0, 0]);
D_pose = actual_tip_vec - target_tip_vec;

% NOTE: Sanity check on dtds_computer.
%fk_check = dtds_computer(-1, -1, angles)

% Tip position
w_pos = 1;
w_ang = 1e-1;
O_position = w_pos*.5*norm(D_position)^2;
O_angle = w_ang*0.5*norm(D_pose)^2;

score = 0;
score = score + O_position;
score = score + O_angle;
%% Compute gradient.
dtds = zeros(3*N, 3);
dads = zeros(3*N, 3);
% NOTE: hacking around Vidya's indexing
roll_i = 2*N+1:3*N;
pitch_i = N+1:2*N;
yaw_i = 1:N;
for i = 1:N
    dtds(roll_i(i), :)  = dtds_computer(i, 1, angles); 
    dtds(pitch_i(i), :) = dtds_computer(i, 2, angles); 
    dtds(yaw_i(i), :)   = dtds_computer(i, 3, angles); 
    
    dads(roll_i(i), :)  = dads_computer(i, 1, angles); 
    dads(pitch_i(i), :) = dads_computer(i, 2, angles); 
    dads(yaw_i(i), :)   = dads_computer(i, 3, angles); 
end
if nargout > 1
    gradient = w_pos*dtds*D_position + w_ang*dads*D_pose;
end


%% DRAW ROBOT
%since this function gets called at each iteration, 
%let this update the drawing
%plot3
global h_axes
global link_lines

for i = 0:N-1
    if i == 0 %base of the first link is at the origin
        pos1 = zeros(3,1);
    else
        pos1 = fk_position(i,1:3);
    end
    pos2 = fk_position(i+1,1:3);
 
    l = link_lines(i+1);
    set(l,'Parent',h_axes,'Xdata',[pos1(1),pos2(1)], ...
        'Ydata',[pos1(2),pos2(2)],'Zdata',[ pos1(3),pos2(3)],'visible','on');
    axis([-5 5 -5 5 -5 5]); 
    
end
global actual_tip_handle target_tip_handle
% DRAW TIP ANGLE: ref in blue, actual in red
t2 = target_tip_pos + target_tip_vec;
a2 = actual_tip_pos + actual_tip_vec;
set(target_tip_handle,'Parent',h_axes,'Xdata',[target_tip_pos(1),t2(1)], ...
        'Ydata',[target_tip_pos(2),t2(2)],'Zdata',[ target_tip_pos(3),t2(3)],'visible','on');
set(actual_tip_handle,'Parent',h_axes,'Xdata',[actual_tip_pos(1),a2(1)], ...
        'Ydata',[actual_tip_pos(2),a2(2)],'Zdata',[ actual_tip_pos(3),a2(3)],'visible','on');
%draw_point_vector(target_tip_pos, target_tip_vec, [0 0 1]);
%draw_point_vector(actual_tip_pos, actual_tip_vec, [1 0 0]);

drawnow
 
end

