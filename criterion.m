function score = criterion( angles )
global link_lengths target_pt
global obstacles
global roll_min roll_max pitch_min pitch_max yaw_min yaw_max
global soft_penalty
%CRITERION Summary of this function goes here
%   Detailed explanation goes here

N = length(link_lengths);
joint_angles_roll = angles(2*N+1:3*N);
joint_angles_pitch = angles(N+1:2*N);
joint_angles_yaw = angles(1:N);
fprintf('yaw = %d\n',length(joint_angles_yaw));
fprintf('roll = %d\n',length(joint_angles_roll));
fprintf('pitch=%d\n',length(joint_angles_pitch));
%joint_angles_yaw = angles(1,:);
%joint_angles_pitch = angles(2,:);
%joint_angles_pitch = angles(3,:);
fk_position = fk(joint_angles_yaw, joint_angles_pitch, joint_angles_roll);

%% Compute score.
tip_quat = target_pt(4:7)';
target_tip_pos = target_pt(1:3);
actual_tip_pos  = fk_position(N,1:3)';

actual_tip_vec = actual_tip_pos - fk_position(N-1,1:3)';
actual_tip_vec = actual_tip_vec / norm(actual_tip_vec);

% Tip position
O_position = norm(actual_tip_pos - target_tip_pos)^2;

% Tip angle
target_tip_vec = quatrotate(tip_quat, [1, 0, 0]);
O_angle = 1e-1*norm(actual_tip_vec - target_tip_vec)^2;

% Euler angle bounds
% O_bounds = 0;
% fit a quadratic min = 1 max = 1 mean = 0
% easier to solve 
rb = zeros(N);
pb = zeros(N);
yb = zeros(N);
for i = 1:N
rm  = vander([roll_min(i) roll_max(i) (roll_min(i)+roll_max(i))/2.]);
pm  = vander([pitch_min(i) pitch_max(i) (pitch_min(i)+pitch_max(i))/2.]);
ym  = vander([yaw_min(i)  yaw_max(i) (yaw_min(i)+yaw_max(i))/2.]);
rhs = [1. 1. 0]';
lhs_r = rm\rhs;
lhs_p = pm\rhs;
lhs_y = ym\rhs;
rb(i) = joint_angles_roll(i).^2*lhs_r(1) + joint_angles_roll(i)*lhs_r(2) + lhs_r(3);
pb(i) = joint_angles_pitch(i).^2*lhs_p(1) + joint_angles_pitch(i)*lhs_p(2) + lhs_p(3);
yb(i) = joint_angles_yaw(i).^2*lhs_y(1) + joint_angles_yaw(i)*lhs_y(2) + lhs_y(3);
end
%needs some playing around with the weight
O_bounds = 1e-3*(norm(rb) + norm(pb) +norm(yb));

% Obstacles
O_obstacles = 0;
for m=1:size(obstacles, 1)
    obstacle = obstacles(m, :)';
    s_obs = obstacle(1:3);
    R = obstacle(4);
    
    for n=1:N
        s_jnt = fk_position(n,1:3)';
        d = norm(s_jnt - s_obs);
        Delta = d - R;
        
        % Simple (sloppy log barrier)
        if (Delta > R)
            %;
        elseif (Delta > 0)
            O_obstacles = O_obstacles - log2(Delta);
        else
            O_obstacles = O_obstacles + 1e6;
        end

    end
end

score = 0;
score = score + O_position;
score = score + O_angle;
if soft_penalty
    score = score + O_bounds;
end
score = score + O_obstacles;

%% DRAW ROBOT
%since this function gets called at each iteration, 
%let this update the drawing
%plot3
global h_axes
global link_lines
disp('criterion')
length(link_lines)

for i = 0:N-1
    if i == 0 %base of the first link is at the origin
        pos1 = zeros(3,1);
    else
        pos1 = fk_position(i,1:3);
    end
    pos2 = fk_position(i+1,1:3);
 
    l = link_lines(i+1);
    set(l,'Parent',h_axes,'Xdata',[pos1(1),pos2(1)], ...
        'Ydata',[pos1(2),pos2(2)],'Zdata',[ pos1(3),pos2(3)],'visible','on');
    axis([-5 5 -5 5 -5 5]); 
    
end
global actual_tip_handle target_tip_handle
% DRAW TIP ANGLE: ref in blue, actual in red
t2 = target_tip_pos + target_tip_vec;
a2 = actual_tip_pos + actual_tip_vec;
set(target_tip_handle,'Parent',h_axes,'Xdata',[target_tip_pos(1),t2(1)], ...
        'Ydata',[target_tip_pos(2),t2(2)],'Zdata',[ target_tip_pos(3),t2(3)],'visible','on');
set(actual_tip_handle,'Parent',h_axes,'Xdata',[actual_tip_pos(1),a2(1)], ...
        'Ydata',[actual_tip_pos(2),a2(2)],'Zdata',[ actual_tip_pos(3),a2(3)],'visible','on');
%draw_point_vector(target_tip_pos, target_tip_vec, [0 0 1]);
%draw_point_vector(actual_tip_pos, actual_tip_vec, [1 0 0]);

drawnow
 
end

