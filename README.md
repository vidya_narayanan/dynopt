# 16-756 Dynamic Optimization Assignment 1 #

Implements forward kinematics for a snake robot. Implements inverse kinematics by optimization
### Files ###

* part1.m implements IK with given function signature
* part2.m implements IK with analytic gradient computation with given function signature
* fk.m implements forward kinematics
* criterion.m implements scoring function for optimization
* criterion_with_gradient.m  implements scoring function and analytical gradient 
* setup.m generates input and algorithm options for testing



### Team Members ###

* James Bern (jbern@andrew.cmu.edu)
* Vidya Narayanan (vidyan@cmu.edu)