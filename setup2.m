clf

% NOTE: Hardcoding p, q for testing.
p = [1 2 1];
q = [-1 .5 -.8 .3];
target = [p q]';

link_len = [1 1 2 1 ];
min_angle  = [-pi -pi -pi -pi];
max_angle  = [ pi  pi pi pi];

obstacles_ = [];

part2(target, link_len, min_angle, max_angle, min_angle, max_angle, min_angle, max_angle, obstacles_);
