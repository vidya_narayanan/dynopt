function tip_position = fk( yaw, pitch, roll )
%FK From joint angles, compute tip positions for 
%the snake robot
%need link lengths
%need raw pitch roll angles per link tip
%compute final position of the tip and its quaternion orientation i.e 
%7 vector x,y,z,q1,q2,q3,q4
%generate tip position for all N links, the final position being that of
%the final link

global link_lengths
N = length(link_lengths);

Rx_ = @(r) [1 0 0 ; 0 cos(r) -sin(r) ; 0 sin(r) cos(r)];
Ry_ = @(p) [cos(p) 0 sin(p) ; 0 1 0 ; -sin(p) 0 cos(p)];
Rz_ = @(y) [cos(y) -sin(y) 0 ; sin(y) cos(y) 0 ; 0 0 1];

cummR = zeros(3,3,N);
for i = 1:N
    y = yaw(i);
    p = pitch(i);
    r = roll(i);
    
    Rx = Rx_(r);
    Ry = Ry_(p);
    Rz = Rz_(y);
    
    R = Rx*Ry*Rz;
    if i == 1
        cummR(:,:,i) = R;
    else
        cummR(:,:,i) = R * cummR(:,:,i-1);
    end
end

tip_position = zeros(N,3);
for i = 1:N
    
    if i == 1
        prev = zeros(3,1);
    else
        prev = tip_position(i-1,:)';
    end

    tip_position(i,:) = prev + cummR(:,:,i)*[link_lengths(i) 0 0]';
    %fprintf('error %d\n',abs(link_lengths(i) - norm(w,2)));
end
 


end

