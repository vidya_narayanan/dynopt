clf

% NOTE: Hardcoding p, q for testing.
p = [1 1 1];
q = [1 0 0 0];

%q = rand(1,4);
%q = q/norm(q,2); %arbitrary quaternion
%p = abs(rand(1,3)); %arbitrary position, positive quadrant for now



%target = [p q]'; 
target = [p q]';
min_angle  = [-pi -pi -pi];
max_angle = [pi pi pi];

% M = ?
obstacles_ = [];%[0 1 1 .25]; %abs(rand(2,4)); % 2 obstacles, not being used but lets just display

% N = 3
link_len = [1 1 1 ];
%run through all the algorithms
global algorithm
global alg_time
global alg_fval
global alg_answer
global soft_penalty
algorithms = {'interior-point','sqp','active-set','cma-es'};
results = {};
soft_penalty = false;
for i = 1:length(algorithms)
    algorithm = algorithms{i};
    name = strcat(algorithm,'-cons.png');
    part1(target, link_len, min_angle, max_angle, min_angle, max_angle, min_angle, max_angle, obstacles_);
    results{end+1} = {algorithm, alg_time, alg_fval, alg_answer};
    saveas(gcf,name);
end
soft_penalty = true;
for i = 1:length(algorithms)
    algorithm = algorithms{i};
    name = strcat(algorithm,'-soft.png');
    part1(target, link_len, min_angle, max_angle, min_angle, max_angle, min_angle, max_angle, obstacles_);
    results{end+1} = {algorithm, alg_time, alg_fval, alg_answer};
    saveas(gcf,name);
end
results
%target