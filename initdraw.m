function [ ] = initdraw( )
%INITDRAW Summary of this function goes here
%   Detailed explanation goes here
%sets up globals for drawing 
global link_lengths link_lines obstacle_spheres 
global h_axes  target_pt target_sphere
global actual_tip_handle target_tip_handle 
global obstacles
disp('init draw');
N = length(link_lengths);
[M,~] = size(obstacles);

animation = figure(1);

set(animation,'name','My 3D Robot','Position',[20 100 500 500]);
h_axes = axes('Parent',animation,'Units','Pixels','Position',[0 0 500 500],'Ylim',[-1.0 1.0],'Xlim',[-1.0 1.0],'Zlim',[-1.,1.]);
tmpAspect=daspect();
daspect(tmpAspect([1 1 1]))
rotate3d
axis(gca,'vis3d');

[tx,ty,tz] = sphere(20);
target_sphere = surf(h_axes,tx*0.1+target_pt(1),ty*0.1+target_pt(2),tz*0.1+target_pt(3)); hold on;
set(target_sphere,'FaceColor',[1 1 0],'FaceAlpha',0.95,'EdgeColor','none','LineStyle','none','FaceLighting','phong');

%tip vectors
target_tip_handle =  plot3(h_axes, [0,1],[1,1],[0,1],'Color',[1,0,0],'Visible','off','LineWidth',1);
actual_tip_handle =  plot3(h_axes, [0,1],[1,1],[0,1],'Color',[0,0,1],'Visible','off','LineWidth',1);


link_lines = [];
obstacle_spheres = [];
for i = 1:N
    c = [rand(), rand(), rand()];
    link_lines = [link_lines, plot3(h_axes, [0,1],[1,1],[0,1],'Color',c,'Visible','off','LineWidth',3)];
    hold on;
    
end
for i = 1:M
    [ox,oy,oz] = sphere(20); 
    r = obstacles(i,4);
    a = obstacles(i,1);
    b = obstacles(i,2);
    c = obstacles(i,3);
    obs = surf(h_axes, r*ox+a,r*oy+b,r*oz+c); hold on;
    set(obs,'FaceColor',[0.5 0.5 0.5],'FaceAlpha',0.95,'EdgeColor','none','LineStyle','none','FaceLighting','phong');
end
set(h_axes,'Visible','on');

end

